package ar.edu.unju.fi.test;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ar.edu.unju.fi.model.Alumno;

public class MainAlumno {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Alumno> listaAlumnos = new ArrayList<>();
		Scanner teclado = new Scanner(System.in);
		int lu,mes,anio;
		cargarLista10(listaAlumnos);
		//cargarAlumno(listaAlumnos,teclado);
		System.out.println(listaAlumnos);
		System.out.println("Ingrese una LU a buscar: ");
		lu = teclado.nextInt();
		buscarLu(listaAlumnos,lu);
		System.out.println("Ingrese un anio: ");
		anio = teclado.nextInt();
		System.out.println("Ingrese un mes: ");
		mes = teclado.nextInt();
		System.out.println("Cantidad de alumnos ingresados en el año "+anio+" y mes "+mes+" : "+contarAlumnosAnioMes(anio,mes,listaAlumnos));
		teclado.close();
	}

	public static void cargarLista10(List<Alumno> listaAlumnos) {
		listaAlumnos.add(new Alumno(1,40330234,"Juan Perez","APU",LocalDate.of(1999, Month.FEBRUARY,2)));
		listaAlumnos.add(new Alumno(2,41234243,"Abril Lopez","APU",LocalDate.of(2000, Month.APRIL,15)));
		listaAlumnos.add(new Alumno(3,38923223,"Alberto Smith","APU",LocalDate.of(1995, Month.FEBRUARY,12)));
		listaAlumnos.add(new Alumno(4,40123123,"Santiago Blanco","APU",LocalDate.of(1998, Month.AUGUST,8)));
		listaAlumnos.add(new Alumno(5,41323478,"Martin Perez","APU",LocalDate.of(2002, Month.JULY,4)));
		listaAlumnos.add(new Alumno(6,39657566,"Leonel Eredia","APU",LocalDate.of(1995, Month.JUNE,25)));
		listaAlumnos.add(new Alumno(7,41231235,"Martina Fernandez","APU",LocalDate.of(2000, Month.OCTOBER,12)));
		listaAlumnos.add(new Alumno(8,42678552,"Facundo Diaz","APU",LocalDate.of(2003, Month.FEBRUARY,4)));
		listaAlumnos.add(new Alumno(9,41245665,"Jorge Alvarez","APU",LocalDate.of(2002, Month.FEBRUARY,11)));
		listaAlumnos.add(new Alumno(10,3812334,"Maria Sanchez","APU",LocalDate.of(1993, Month.FEBRUARY,14)));
	}
	
	public static LocalDate ingresarFecha(Scanner teclado) {
		LocalDate fecha = LocalDate.now();
		String fechaIngresada;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		do {
			System.out.println("Ingrese una fecha (dd/MM/yyyy) : ");
			fechaIngresada = teclado.nextLine();
			try {
				fecha = LocalDate.parse(fechaIngresada,formatter);
			} catch (Exception e) {
		        System.out.println("Formato de fecha inválido. Asegúrate de ingresarla en el formato dd/MM/yyyy.");
		        fechaIngresada = "0";
		    }
		}while(fechaIngresada == "0");
		return fecha;
	}
	
	public static void cargarAlumno(List<Alumno> listaAlumnos,Scanner teclado) {
		Alumno alum = new Alumno();
		int lu,dni;
		String nombre,carrera;
		LocalDate fechaNacimiento,fechaIngreso;
		System.out.println("Cargar datos del alumno\n");
		System.out.println("Ingrese el lu: ");
		lu = teclado.nextInt();
		System.out.println("Ingrese el dni: ");
		dni = teclado.nextInt();
		teclado.nextLine();//Lo uso para que tome el /n que queda en el buffer.
		System.out.println("Ingrese el nombre: ");
		nombre = teclado.nextLine();
		System.out.println("Ingrese la carrera: ");
		carrera = teclado.nextLine();
		System.out.println("Ingrese la fecha de nacimiento");
		fechaNacimiento = ingresarFecha(teclado);
		System.out.println("Ingrese la fecha de ingreso");
		fechaIngreso = ingresarFecha(teclado);
		alum.setLu(lu);
		alum.setDni(dni);
		alum.setNombre(nombre);
		alum.setCarrera(carrera);
		alum.setFechaNacimiento(fechaNacimiento);
		alum.setFechaIngreso(fechaIngreso);
		listaAlumnos.add(alum);
	}
	
	public static void buscarLu(List<Alumno> listaAlumnos,int lu) {
		boolean encontrado=false;
		int i=0;
		for(i=0;i<listaAlumnos.size() && !encontrado;i++) {
			if(listaAlumnos.get(i).getLu() == lu) {
				encontrado = true;
			}
		}
		if(encontrado) {
			i--;
			System.out.println("Alumno encontrado.");
			System.out.println(listaAlumnos.get(i));
		}else {
			System.out.println("Alumno NO encontrado.");
		}
	}
	
	public static int contarAlumnosAnioMes(int anio,int mes,List<Alumno> listaAlumnos) {
		int c=0,mesIngresado,anioIngresado;
		for(Alumno alum : listaAlumnos) {
			mesIngresado = alum.getFechaIngreso().getMonthValue();
			anioIngresado = alum.getFechaIngreso().getYear();
			System.out.println("Anio "+anioIngresado+" mes "+mesIngresado);
			if(anioIngresado == anio && mesIngresado == mes) {
				c++;
			}
		}
		return c;
	}
}
