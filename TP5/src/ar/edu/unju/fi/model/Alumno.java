package ar.edu.unju.fi.model;

import java.time.LocalDate;

public class Alumno {
	private int lu;
	private int dni;
	private String nombre;
	private String carrera;
	private LocalDate fechaNacimiento;
	private LocalDate fechaIngreso;
	
	public Alumno() {
		
	}
	
	public Alumno(int lu,int dni,String nombre) {
		this.lu = lu;
		this.dni = dni;
		this.nombre = nombre;
	}

	public Alumno(int lu, int dni, String nombre, String carrera, LocalDate fechaNacimiento) {
		super();
		this.lu = lu;
		this.dni = dni;
		this.nombre = nombre;
		this.carrera = carrera;
		this.fechaNacimiento = fechaNacimiento;
		this.fechaIngreso = LocalDate.now();
	}

	public int getLu() {
		return lu;
	}

	public void setLu(int lu) {
		this.lu = lu;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public LocalDate getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(LocalDate fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	@Override
	public String toString() {
		return "Alumno [lu=" + lu + ", dni=" + dni + ", nombre=" + nombre + ", carrera=" + carrera
				+ ", fechaNacimiento=" + fechaNacimiento + ", fechaIngreso=" + fechaIngreso + "]";
	}
}
